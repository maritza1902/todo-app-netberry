<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Task;
use App\Entity\Tag;
use App\Entity\TaskTag;
use Symfony\Component\HttpFoundation\Request;

class TaskController extends AbstractController
{

  /**
   * @Route("/tasks", name="tasks")
   */
  public function list()
  {
    $tasks = $this->getDoctrine()->getRepository(Task::class)->findAll();

    foreach ($tasks as $task) {

      $tasgs_array = [];

      foreach ($task->getTags() as $tag) {

        $tasgs_array[] = [
          'id' => $tag->getTag()->getId(),
          'name' => $tag->getTag()->getName()
        ];
      }

      $task_array[] = [
        'id' => $task->getId(),
        'name' => $task->getName(),
        'tags' => $tasgs_array
      ];
    }

    return $this->json([
      'tasks' => isset($task_array) ? $task_array : []
    ]);
  }

  /**
   * @Route("/task", name="task_add", methods={"POST"})
   */
  public function add(Request $request)
  {
    $em = $this->getDoctrine()->getManager();

    $task = new Task();
    $task->setName($request->get('name'));
    $em->persist($task);
    $em->flush();

    foreach (json_decode($request->get('tags'), TRUE) as $tag) {

      $taskTag = new TaskTag();
      $taskTag->setTask($task);
      $taskTag->setTag($this->getDoctrine()->getRepository(Tag::class)->find($tag['id']));
      $em->persist($taskTag);
      $em->flush();

      $task->addTag($taskTag);
    }

    $tasgs_array = [];

    foreach ($task->getTags() as $tag) {

      $tasgs_array[] = [
        'id' => $tag->getTag()->getId(),
        'name' => $tag->getTag()->getName()
      ];
    }

    $task_array = [
      'id' => $task->getId(),
      'name' => $task->getName(),
      'tags' => $tasgs_array
    ];

    return $this->json(
      [
        'task' => $task_array
      ]
    );
  }

  /**
   * @Route("/task/{id}", name="task_delete", methods={"DELETE"})
   */
  public function delete($id)
  {
    $em = $this->getDoctrine()->getManager();
    $task = $em->getRepository(Task::class)->find($id);

    if (!$task) {
      throw $this->createNotFoundException(
        'No task found for id ' . $id
      );
    }

    $em->remove($task);
    $em->flush();

    return $this->json(
      [
        'data' => 'Eliminado con exito'
      ]
    );
  }
}
