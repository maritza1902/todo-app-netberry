<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Tag;

class TagController extends AbstractController
{

  /**
   * @Route("/tags", name="tags")
   */
  public function list()
  {
    $tags = $this->getDoctrine()->getRepository(Tag::class)->findAll();

    foreach ($tags as $task) {

      $task_array[] = [
        'id' => $task->getId(),
        'name' => $task->getName()
      ];
    }

    return $this->json(
      [
        'tags' => $task_array
      ]
    );
  }
}
