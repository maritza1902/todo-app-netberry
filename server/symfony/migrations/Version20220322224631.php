<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220322224631 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
      $this->addSql("INSERT INTO `task` (`id`, `name`) VALUES (NULL, 'Preparación de entorno de desarrollo')");
      $this->addSql("INSERT INTO `task` (`id`, `name`) VALUES (NULL, 'Creación de entidades')");
      $this->addSql("INSERT INTO `tag` (`id`, `name`) VALUES (NULL, 'php')");
      $this->addSql("INSERT INTO `tag` (`id`, `name`) VALUES (NULL, 'javascript')");
      $this->addSql("INSERT INTO `tag` (`id`, `name`) VALUES (NULL, 'css')");

        // this up() migration is auto-generated, please modify it to your needs

    }
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
