# NetBerry Reto

Solution of the challenge proposed by NetBerry

## Requirements 
- Docker
- Docker-compose

## Execute
- From root: ``` docker-compose up ```

## Down
- From root: ``` docker-compose down -v ```
 