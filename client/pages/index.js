import { useEffect, useState } from "react";
import Head from 'next/head'
import Form from "../components/form";
import Table from "../components/table";
import axios from "axios";

export default function Home() {
  const [loading, setLoading] = useState(true);
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8001/tasks").then(res => {
      setTasks(res.data.tasks);
      setLoading(false);
    })
      .catch(err => {
        console.log(err);
        setLoading(false);
      });
  }, []);

  const deleteTodo = (id) => {
    axios.delete(`http://localhost:8001/task/${id}`).then(res => {
      setTasks(tasks.filter(task => task.id !== id));
      setLoading(false);
    }).catch(err => {
      console.log(err);
      setLoading(false);
    });
  };

  const addTodo = (task) => {
    const params = new URLSearchParams(task);

    axios.post("http://localhost:8001/task", params).then(res => {
      setTasks([...tasks, res.data.task]);
      setLoading(false);
    }).catch(err => {
      console.log(err);
      setLoading(false);
    });
  };

  console.log(tasks);

  if (loading) {
    return <p>Loading...</p>;
  }

  return (
    <>
      <Head>
        <title>Gestor de Tareas</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="flex flex-col font-body min-h-screen max-w-5xl mx-auto px-6 py-10 ">
        <h1 className="text-3xl text-gray-800 font-bold tracking-[0.25rem] text-left px-6">
          Gestor de tareas
        </h1>
        <div className="border border-gray-800 mx-6 my-3">
        </div>
        <Form addTodo={addTodo} />
        <Table tasks={tasks} deleteTodo={deleteTodo} />
      </div>
    </>
  )
}
