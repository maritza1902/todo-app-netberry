// checked:bg-gradient-to-br checked:from-check-start checked:to-check-end

export default function Checkbox({ name, type }) {
  return (
    <div className="flex flex-row items-center">
      <input
        value={type}
        name={name}
        type="checkbox"
        className="accent-pink-500 mx-2 rounded-sm focus:outline-none focus:ring-0"
      />
      {type}
    </div>

  );
}
