import { useEffect, useState } from "react";
import axios from "axios";
import Checkbox from "./checkbox";

export default function Form({ addTodo }) {

  // const type = ['PHP', 'JavaScript', 'CSS']

  const [loading, setLoading] = useState(true);
  const [tags, setTags] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8001/tags").then(res => {
      setTags(res.data.tags);
      setLoading(false);
    }).catch(err => {
      console.log(err);
      setLoading(false);
    });

  }, []);


  if (loading) {
    return <p>Loading...</p>;
  }


  const createTodo = (event) => {
    event.preventDefault();
    console.log(event.target.task.value);

    if (!event.target.task.value) return;

    let taskName = event.target.task.value;
    let tagsSelecteds = [];

    tags.map(item => {
      console.log(item);
      if (event.target[item.name].checked) {
        tagsSelecteds.push(item)
      }
      event.target[item.name].checked = false;
    })

    event.target.task.value = "";

    let task = {
      name: taskName,
      tags: JSON.stringify(tagsSelecteds)
    }

    addTodo(task);

  };

  return (
    <form
      className="w-full flex items-center bg-white dark:bg-gray-800 rounded-md px-7 h-10 sm:h-12 space-x-1"
      onSubmit={createTodo}
    >
      <input
        type="text"
        className="basis-3/6 text-xs sm:text-base bg-white dark:bg-gray-800 dark:text-gray-100 focus:outline-none focus:ring-0 rounded-md"
        placeholder="Nueva tarea..."
        name="task"
      />
      <div className="basis-2/6 justify-between px-5 flex flex-row">
        {tags.map(item => (
          <Checkbox name={item.name} type={item.name} key={item.id} />
        ))}
      </div>
      <div className="basis-1/6 justify-end flex flex-row">
        <button type="submit" className="bg-gray-800 dark:bg-gray-600 text-white rounded-md px-4 py-2">
          Añadir
        </button>
      </div>
    </form>
  );
}
