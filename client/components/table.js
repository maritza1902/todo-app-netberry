export default function Table({ tasks, deleteTodo }) {

  return (
    <table className="table-auto mx-7 my-10 border-collapse border border-gray-400 font-body">
      <thead>
        <tr className="bg-gray-300 h-10">
          <th className=" pl-5 text-left">Tarea</th>
          <th className=" pl-5 text-left">Categorías</th>
          <th className=" pl-5 text-center">Acciones</th>
        </tr>
      </thead>
      <tbody>
        {tasks.map(task => (
          <tr key={task.id}>
            <td className="pl-5 ">{task.name}</td>
            <td className="pl-5  flex flex-row px-5">{task.tags.map(tag => (
              <span key={tag.id}
                className="my-4 px-4 py-2 mr-2 rounded-full text-gray-700 bg-slate-200 font-semibold text-sm align-center w-max">
                {tag.name}
              </span>
            ))}</td>
            <td className="pl-5 text-center pr-2">
              <button className="bg-red-700 text-white rounded-md px-4 py-2" onClick={() => deleteTodo(task.id)}>
                X
              </button>
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  )
}