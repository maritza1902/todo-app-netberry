import axios from "axios";

export default async function sendQuery(path,method,data,variables) {
  const res = await axios({
    url: "http://localhost:8001/"+path,
    method: method,
    data: {
      data,
      variables,
    },
  });

  return res.data;
}
